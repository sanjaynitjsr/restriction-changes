 
package web_elements_definition;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class Login_Page_webelements {

        By username= By.name("username");
        By password =By.name("password");
        By submitbutton=By.xpath("//button[@type='submit']");
        By Menulink=By.id("space-menu-link");
       
       
        WebDriver driver = Test_Flow_and_Test_Parameters.Test_Parameters.getDriver("FirefoxDriver");
        
        public void typeUserName(String uid)
        {
        	driver.findElement(username).sendKeys(uid);
        }	
        
        public void typePassword(String pwd)
        {
        	driver.findElement(password).sendKeys(pwd);
        }
        
        
        public void ClickLoginButton()
        {
        	driver.findElement(submitbutton).click();
        }
        
        
        
	}
      



