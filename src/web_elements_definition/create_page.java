 
package web_elements_definition;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;



public class create_page {


	WebDriver driver = Test_Flow_and_Test_Parameters.Test_Parameters.getDriver("FirefoxDriver");
  
        
        By display_Spaces=By.id("space-menu-link");
        By Select_Space= By.linkText("sanjaymukherji");
        By Create_Page= By.linkText("Create");
        By Add_Title=By.id("content-title");
        By Submit_Page= By.xpath("//button[@type='submit']");
        
  
        
        public void Show_all_spaces()
        {
        	driver.findElement(display_Spaces).click();
        	
        }	
        
        public void Select_from_spaces()
        {
        	driver.findElement(Select_Space).click();
        }
        
        
        public void Create_a_Page()
        {
        	driver.findElement(Create_Page).click();
        	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        }
        
        public void Add_a_Title(String tit )
        {
        	driver.findElement(Add_Title).sendKeys(tit);
        }
        
        public void Submit_a_Page()
        {
        	driver.findElement(Submit_Page).click();
        }
        
            
	}
      



