package web_elements_definition;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class All_Processes_webelements {
	
	 WebDriver driver = Test_Flow_and_Test_Parameters.Test_Parameters.getDriver("FirefoxDriver");
	By username= By.name("username");
    By password =By.name("password");
    By submitbutton=By.xpath("//button[@type='submit']");
    By Menulink=By.id("space-menu-link");
    
    By display_Spaces=By.id("space-menu-link");
    By Select_Space= By.linkText("sanjaymukherji");
    By Select_Content= By.linkText("space-menu-link-content");
    By Select_Pages= By.linkText("Pages");
    By Particular_Pages= By.linkText("This One");
    By Page_RestrictionsPage=By.id("content-metadata-page-restrictions");
    static By SelectDrop_down_menu=By.className("select2-choice");
	By no_restictions=By.xpath(".//*[@id='select2-drop']/ul/li[1]/div/div/span[2]");
	
	By restriction_type=By.xpath(".//*[@id='update-page-restrictions-dialog']/div/div[1]/span[2]/span[1]");
	
	By is_editable=By.xpath(".//*[@id='editPageLink']/span");
	
	By Logoutdrop_down=By.xpath(".//*[@id='main-content']/p[1]");
	By profile_link=By.xpath(".//*[@id='view-user-profile-link']");
	By auth_level=By.xpath(".//*[@id='title-text']");
	

	//@Test
	
	public void typeUserName(String uid)
    {
    	driver.findElement(username).sendKeys(uid);
    }	
    
    public void typePassword(String pwd)
    {
    	driver.findElement(password).sendKeys(pwd);
    }
    
    
    public void ClickLoginButton()
    {
    	driver.findElement(submitbutton).click();
    }
    
    
    public void SelectSpace()
    {
    	driver.findElement(Select_Space).click();;
    }	
    public void Select_Pages()
    {
    	driver.findElement(Select_Pages).click();
    }	
    
    
    public void Select_Pages_old()
    {
    	driver.findElement(Select_Pages).sendKeys();
    }	
    
    // Add timer 
    
    public void Particular_Pages()
    {
    	driver.findElement(Particular_Pages).click();;
    }	
    
    public void Page_RestrictionsPage()
    {
    	driver.findElement(Page_RestrictionsPage).click();;
    }	
    
    
    public void SelectDrop_down_menu()
    {
    	driver.findElement(SelectDrop_down_menu).click();;
    }	
    
    
public String Find_Restriction_Type()
	
	{
	
	String restype=driver.findElement(restriction_type).getText();
//	System.out.println(restype);
	return restype;
	}


public String Find_Editable_or_Viewable1  ()

{	
String editbleor_not = null;
//Boolean editable_element_present=driver.findElement(is_editable).isDisplayed();
//int editable_element_present=driver.findElements(is_editable).size();
try
{
	if (driver.findElement(is_editable).isDisplayed())
	{
		editbleor_not=driver.findElement(getIs_editable()).getText();	
	}
}
catch(Exception e)
{
	editbleor_not="non_editable";
}

System.out.println(editbleor_not);
return editbleor_not;

}


    

public void Click_Logout_Drop_Down()
{
	driver.findElement(Logoutdrop_down).click();;
}	

public void Click_user_profile()
{
	driver.findElement(profile_link).click();
}	
        

public String Find_User_Authorization()

{

String auth_level_user=driver.findElement(auth_level).getText();
//System.out.println(editbleor_not);
return auth_level_user;
}


public void Add_a_timer()
{
	
	driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
}


public By getIs_editable() {
	return is_editable;
}

public void setIs_editable(By is_editable) {
	this.is_editable = is_editable;
}

	


    	
    	
        
       
        
       
       


}
