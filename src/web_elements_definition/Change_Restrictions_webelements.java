package web_elements_definition;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public  class Change_Restrictions_webelements {
	
	WebDriver driver = Test_Flow_and_Test_Parameters.Test_Parameters.getDriver("FirefoxDriver");
	By no_restictions=By.xpath(".//*[@id='select2-drop']/ul/li[1]/div/div/span[2]");
	By edit_restictions=By.xpath(".//*[@id='select2-drop']/ul/li[2]/div/div/span[2]");
	By editand_view_restictions=By.xpath(".//*[@id='select2-drop']/ul/li[3]/div/div/span[2]");
	By apply_page_restictions=By.id("page-restrictions-dialog-save-button");
	By SelectDrop_down_menu=By.className("select2-choice");
	
	
	public void Change_Restrictions_No_Restrictions()
	{
		
		    driver.findElement(no_restictions).click();
			
	}
	
	public void Change_Restrictions_editing_Restricted()
	{
	
    driver.findElement(edit_restictions).click();
    
    
	}
	
	public void Change_Restrictions_viewing_editing_restricted()
	
	{
    driver.findElement(editand_view_restictions).click();
	}
	
	
public void Apply_Restriction()
	
	{
     driver.findElement(apply_page_restictions).click();
  
	}


    
}

