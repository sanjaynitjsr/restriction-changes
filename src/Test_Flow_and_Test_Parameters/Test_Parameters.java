package Test_Flow_and_Test_Parameters;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class Test_Parameters {
	
	private static WebDriver driver = null;
	
	public static int Iteration_number=5;
	
	
	
	public static WebDriver getDriver(String browserName)
	{
		if(driver == null)
		{
			if(browserName.equals("InternetExplorer"))
			{
				driver = new InternetExplorerDriver();
			}
			else if(browserName.equals("FirefoxDriver"))
			{
				driver = new FirefoxDriver();
			}
			else if(browserName.equals("Chrome"))
			{
				driver = new ChromeDriver();
			}
			
		}		
		return driver;
	}
	

}
