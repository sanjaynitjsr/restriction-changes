package High_Level_Test_Flow;
import java.io.IOException;
import org.testng.annotations.Test;
import Test_Flow_and_Test_Parameters.Test_Parameters;
import web_elements_definition.All_Processes_webelements;
import Collection_of_processes.Change_Restriction_individual_processes_and_test_cases;


public class Change_and_Check_Restriction_High_Level_Test_Flow extends Test_Parameters {
	
	public int Get_Iteration_number()
	{
		int numberof_iterations;
		return numberof_iterations=Test_Parameters.Iteration_number;
	}
	
	
	@Test
	public void Change_Restrctions_Process_Call() throws IOException, InterruptedException {
		
		
		String page_authoriziation;/* String defined to capture Restriction Level set by Admin in 
		Round robin fashion on each iteration */
		
		String actual_page_auth; /* String Defined to Capture whether Page is non viewable, viewable
		or editable */
		
		String user_auth_level;/* String defined to capture if user is Admin or Non Admin */
		
		/* while loop defines number of iterations , on each iteration the Page restriction 
		 * type is changed in a round robin way*/ 
		 
		int i = 0;
		int number_of_iterations;
		number_of_iterations=this.Get_Iteration_number();
		while(i<number_of_iterations)
		{
			System.out.println("Starting Iteration number");
			System.out.println(i);
			
		Change_Restriction_individual_processes_and_test_cases All_Pages_Change_Restrictions = new Change_Restriction_individual_processes_and_test_cases();
		All_Processes_webelements All_restrictions_new_test=new All_Processes_webelements();
		
		All_Pages_Change_Restrictions.LoginPage();
		All_restrictions_new_test.Add_a_timer();	
		user_auth_level=All_Pages_Change_Restrictions.Find_User_Auth_Level();
			
		System.out.println(user_auth_level);
	
	//  Administrator Logs IN to change Restriction Type in a Round Robin Fashion , rest sections are by General User 	
		All_Pages_Change_Restrictions.Leading_To_Page_Restriction_Dropdown();
	
		All_restrictions_new_test.Add_a_timer();
		page_authoriziation=All_Pages_Change_Restrictions.Change_Current_Restrition_Level();
		
		Thread.sleep(3000);
	
		All_Pages_Change_Restrictions.LogOutPage();
    // Administrator Logs out and Rest actions are done by General user. 
	
		
		actual_page_auth=All_Pages_Change_Restrictions.Able_to_View();
		
		System.out.println(actual_page_auth);
		
		String concatstring;
		System.out.println("The actual authorization for the page is   :" + actual_page_auth);
		System.out.println("The User Authorization level is " +user_auth_level);
		System.out.println("The Page Authorization set by the script in round robin fashsion  :" + page_authoriziation);
		concatstring=actual_page_auth.concat(user_auth_level).concat(page_authoriziation);
	
	/* Concatinating the above three , only a set of combinations would when concatinated would 
	 * be passed test conditions
	 *  
	 * The List of Combinations that would pass are , any other combination would be a fail condition. 
	 *   |User Type|    |Page Restriction Set|          |If Page is Visible or Editable| ( This should be based on the first two and accordingly the test condition of passes or fails)
	 *  1. admin       Everyone can view and edit        editable
	 *  2. admin       some can view and edit            editable
	 *  3. admin       Everyone can view some can edit   editable
	 *  4. non admin   Everyone can view and edit        editable
	 *  5. non admin   some can view and edit            (viewable and editable: depending upon where the user is defined )
	 *  6. non admin   Everyone can view some can edit    viewable, (editable: depending upon where the user is defined )
	 * 
	 * Any other combination would be be a failed test case . 
	 * 
	 */
		
		if (concatstring.indexOf("Administrator")>=0) 
		{concatstring=actual_page_auth.concat("Admin").concat(page_authoriziation);
			;}
		else 
		{concatstring=actual_page_auth.concat("NonAdmin").concat(page_authoriziation);}
		
		System.out.println(concatstring);
		
		switch(concatstring)
	      {
	         case "EditAdminEveryone can view and edit this page.":
	        	
	 			System.out.println("Passed Condition Final");
	            break;
	         case "EditAdminEveryone can view, only some can edit.":
	        	
	 			System.out.println("Passed Condition Final");
	            break; 
	         case "EditAdminOnly some people can view or edit.":
	        	
	 			System.out.println("Passed Condition Final");
	            break;
	            
	         case "EditNonAdminEveryone can view and edit this page.":
		        	
		 			System.out.println("Passed Condition Final");
		            break;
		         case "EditNonAdminEveryone can view, only some can edit.":
		        	
		 			System.out.println("Passed Condition Final");
		            break; 
		         case "EditNonAdminOnly some people can view or edit.":
		        	
		 			System.out.println("Passed Condition Final");
		            break;
	         
	      
	         default :
	            System.out.println("Any other condition apart from above are failed hence ,Failed!");
	    
	      }
	
		i=i+1;
	  }
		
		
	}

}
