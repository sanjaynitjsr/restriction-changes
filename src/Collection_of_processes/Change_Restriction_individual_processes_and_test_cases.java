package Collection_of_processes;

import org.openqa.selenium.WebDriver;
import web_elements_definition.Add_Timer_webelement;
import web_elements_definition.All_Processes_webelements;
import web_elements_definition.Change_Restrictions_webelements;
import web_elements_definition.Login_Page_webelements;
import web_elements_definition.Logout_Page_webelements;


public class Change_Restriction_individual_processes_and_test_cases extends All_Processes_webelements {
	
	

	/* Login with General user */
	
	public void LoginPage_otheruser() {
		
		WebDriver driver = Test_Flow_and_Test_Parameters.Test_Parameters.getDriver("FirefoxDriver");
		
		driver.manage().window().maximize();
		driver.get("https://sanjaymukherji.atlassian.net/login");
		
		Login_Page_webelements login=new Login_Page_webelements();
		login.typeUserName("sanjaymukherji7@gmail.com");
		login.typePassword("sanjay84");
		login.ClickLoginButton();

	}
	
	/*Admin Login*/
	public void LoginPage_admin() {
		
		WebDriver driver = Test_Flow_and_Test_Parameters.Test_Parameters.getDriver("FirefoxDriver");
		
		driver.manage().window().maximize();
		driver.get("https://sanjaymukherji.atlassian.net/login");
		
		Login_Page_webelements login=new Login_Page_webelements();
		login.typeUserName("sanjaynitjsr@gmail.com");
		login.typePassword("Durmil@25april");
		login.ClickLoginButton();

	}
	
	/* Calls home Page and Logs out( Any user from anywhere)
	 */
	public void LogOutPage() {
		
		Logout_Page_webelements Logout=new Logout_Page_webelements();
		Logout.Click_Home_Page();
		Logout.Click_Logout_Link();
		Logout.Click_Logout_Button();
		Logout.Click_Post_Logout();
	}
	
/* General user logs out and Admin Users Logs in to Change the Restriction type , the below funtion leads
 to the Change Restriction dropdown
 */

public void Leading_To_Page_Restriction_Dropdown() throws InterruptedException {
	
	All_Processes_webelements All_restrictions_new_test=new All_Processes_webelements();
	Add_Timer_webelement Add_Timer = new Add_Timer_webelement();


	this.LogOutPage();
	this.LoginPage_admin();
	
	All_restrictions_new_test.SelectSpace();
	All_restrictions_new_test.Select_Pages();
	
	All_restrictions_new_test.Particular_Pages();
	Add_Timer.Add_a_timer();
	Thread.sleep(3000); /* Have added fixed wait times only waiting 
	for Asynchronous Javascript component, ideally should add some Javascript snippet to handle this. 
	*/	
	
	All_restrictions_new_test.Page_RestrictionsPage();
	Thread.sleep(3000);
	Thread.sleep(3000);
	All_restrictions_new_test.SelectDrop_down_menu();
	
}

	
/* Selecting a particular page to change its restriction */

public void Other_part1() {
		
		All_Processes_webelements All_restrictions_new_test=new All_Processes_webelements();
		Add_Timer_webelement Add_Timer = new Add_Timer_webelement();
		Logout_Page_webelements Logout=new Logout_Page_webelements();
		Logout.Click_Home_Page();
		All_restrictions_new_test.SelectSpace();
		All_restrictions_new_test.Select_Pages();
		Add_Timer.Add_a_timer();
		
		All_restrictions_new_test.Particular_Pages();
		
	}
/* Admin User Changes Restriction to No Restrction
 */
	public void No_Restrictions() {	
		
		Change_Restrictions_webelements Change_Restrictions=new Change_Restrictions_webelements();
		Change_Restrictions.Change_Restrictions_No_Restrictions();
	}
	
/* Admin User Changes Restriction to Editing is Restricted
	 */	
public void Editing_Restrictions() {
		
		Change_Restrictions_webelements Change_Restrictions=new Change_Restrictions_webelements();
        Change_Restrictions.Change_Restrictions_editing_Restricted();
	}
/* Admin User Changes Restriction to Viewing is Restricted
 */	
public void Viewing_Restrictions() {
	
	Change_Restrictions_webelements Change_Restrictions=new Change_Restrictions_webelements();
	Change_Restrictions.Change_Restrictions_viewing_editing_restricted();
}


/* Admin User Applies the above Changed Restrictions
 */	
public void Apply_Restrictions() {
	
	Change_Restrictions_webelements Change_Restrictions=new Change_Restrictions_webelements();
	Change_Restrictions.Apply_Restriction();

}

/*
 The Function below changes the Restriction type according to the Current Restriction. 
 It does this in a round robin fashion. ( This is done by the Admin User ) 
 */


public String Change_Current_Restrition_Level() {
String restrictiontype;
String restriction_Flag;
String Set_Current_Restriction;	
	All_Processes_webelements All_restrictions_new_test=new All_Processes_webelements();
	
	restrictiontype=All_restrictions_new_test.Find_Restriction_Type();
	restriction_Flag=restrictiontype;
	  switch(restrictiontype)
      {
         case "Only some people can view or edit.":
        	 this.No_Restrictions();
 			this.Apply_Restrictions();
 			Set_Current_Restriction="No_Restriction";
            break;
         case "Everyone can view and edit this page.":
        	 this.Editing_Restrictions();
 			this.Apply_Restrictions();
 			Set_Current_Restriction="Editing_Restriction";
            break; 
         case "Everyone can view, only some can edit.":
        	 this.Viewing_Restrictions();
 			this.Apply_Restrictions();
 			Set_Current_Restriction="Viewing_Restriction";
            break;
      
         default :
            System.out.println("Restrictions have to be one of those 3 , please check");
      }
   
	return restriction_Flag;
}


public String Editable_or_not() {		
String is_editable;
 String editable_flag;	
	All_Processes_webelements All_restrictions_new_test=new All_Processes_webelements();
	is_editable=All_restrictions_new_test.Find_Editable_or_Viewable1();
	editable_flag=is_editable;
	return editable_flag;
}

// Finding out Current User Authorization Level with respect to the Page. 
public String Find_User_Auth_Level() {
String user_auth_level;
	
	All_Processes_webelements All_restrictions_new_test=new All_Processes_webelements();
	Logout_Page_webelements Logout=new Logout_Page_webelements();
	Logout.Click_Home_Page();
	Logout.Click_Logout_Link();
	All_restrictions_new_test.Click_user_profile();
	user_auth_level=All_restrictions_new_test.Find_User_Authorization();

	return user_auth_level;
}


public void Main_Testing_Function() {
	String get_value;
	
	get_value=this.Editable_or_not();
	System.out.println(get_value);
	
}

public void LogOutPage1() {
	
	Logout_Page_webelements Logout=new Logout_Page_webelements();
	
	Logout.Click_Home_Page();
	Logout.Click_Logout_Link();
	Logout.Click_Logout_Button();
	Logout.Click_Post_Logout();
}


public void LoginPage() {
	
	WebDriver driver = Test_Flow_and_Test_Parameters.Test_Parameters.getDriver("FirefoxDriver");
	
	driver.manage().window().maximize();
	driver.get("https://sanjaymukherji.atlassian.net/login");
	
	Login_Page_webelements login=new Login_Page_webelements();
	login.typeUserName("sanjaynitjsr@gmail.com");
	login.typePassword("Durmil@25april");
	login.ClickLoginButton();

}
// Find out if the Page is viewable , non viewable , editable or non editable. 
public String Able_to_View() {
	
	String editable_firstlevel;	
	
	Change_Restriction_individual_processes_and_test_cases All_Pages_Change_Restrictions = new Change_Restriction_individual_processes_and_test_cases();
	All_Processes_webelements All_restrictions_new_test=new All_Processes_webelements();
	All_Pages_Change_Restrictions.LoginPage();
	All_restrictions_new_test.Add_a_timer();
	All_Pages_Change_Restrictions.Other_part1();
	All_restrictions_new_test.Add_a_timer();
	editable_firstlevel=All_Pages_Change_Restrictions.Editable_or_not();
	All_Pages_Change_Restrictions.LogOutPage();
	
	return editable_firstlevel;
}




	
}
